﻿using LamdaPractice.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            
            using (var ctx = new DatabaseContext())
            {
                Console.WriteLine("Listar todos los empleados cuyo departamento tenga una sede en Chihuahua");
                var cdChihuahua = ctx.Cities.Where(a => a.Name == "Chihuahua").ToList();
                var idChihuahua = ctx.Cities.First(a => a.Name == "Chihuahua");
                
                foreach (var departamento in ctx.Departments.ToList())
                {
                    try
                    {
                        foreach (var ciudad in departamento.Cities.ToList())
                        {
                            if (ciudad.Name == "Chihuahua")
                            {
                                ctx.Employees.ToList().FindAll(a => a.DepartmentId == departamento.Id).ForEach(b => Console.WriteLine(b.FirstName + " " + b.LastName + " del departamento numero " + b.DepartmentId));
                            }
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }

                Console.WriteLine("Listar todos los departamentos y el numero de empleados que pertenezcan a cada departamento.");
                ctx.Departments.ToList().ForEach(a => Console.WriteLine(a.Name +" " + ctx.Employees.Where(b => b.DepartmentId == a.Id).Count()));
                Console.WriteLine("Listar todos los empleados remotos. Estos son los empleados cuya ciudad no se encuentre entre las sedes de su departamento.");
                bool bandera= true;
                foreach (var empleados in ctx.Employees.ToList())
                {
                    try
                    {
                        var idCiudad = ctx.Departments.Find(empleados.DepartmentId);
                        foreach (var ciudad in idCiudad.Cities.ToList())
                        {
                            if (empleados.CityId == ciudad.Id)
                            {
                                bandera = false;
                            }

                        }
                        if (bandera)
                        {
                            Console.WriteLine(""+empleados.FirstName+" "+empleados.LastName+" es foraneo");
                            bandera = true;
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                Console.WriteLine("Listar todos los empleados cuyo aniversario de contratación sea el próximo mes.");
                ctx.Employees.ToList().FindAll(a => a.HireDate.Month == 5).ForEach(a => Console.WriteLine(a.FirstName + " " + a.LastName));
                Console.WriteLine("Listar los 12 meses del año y el numero de empleados contratados por cada mes.");
                ctx.Employees.GroupBy(a => a.HireDate.Month).Select(b => new { ID = b.Key, count = b.Count() }).AsEnumerable().OrderBy(c => c.ID).ToList().ForEach(d => Console.WriteLine(d.ID +"  " +d.count));
            }


            Console.Read();
        }
    }
}
